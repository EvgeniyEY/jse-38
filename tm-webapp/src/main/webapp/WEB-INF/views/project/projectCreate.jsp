<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../resources/_prePage.jsp"/>

<p class="font" style="margin: 0px">PROJECT CREATE</p>

<form:form method="post" action="/projects/create" modelAttribute="project">
    <div style="margin-top: 5px">Enter project name:</div>
    <input type="text" name="name" value="name"/><br/>
    <div style="margin-top: 5px">Enter project description:</div>
    <input type="text" name="description" value="description"/><br/>
    <div style="margin-top: 5px">Choose user for this project:</div>
    <select name="user_id">
        <c:forEach items="${users}" var="user">
            <option value=<c:out value="${user.id}"/>><c:out value="${user.login}"/></option>
        </c:forEach>
    </select>
    <div>
        <input class="button" style="margin-top: 10px" type="submit" value="Create project"/>
    </div>
</form:form>

<jsp:include page="../resources/_postPage.jsp"/>