package ru.ermolaev.tm.constant;

public final class DataConstant {

    public static final String FILE_BINARY = "./data/data.bin";

    public static final String FILE_BASE64 = "./data/data.base64";

    public static final String FILE_XML_FX = "./data/data_fx.xml";

    public static final String FILE_JSON_FX = "./data/data_fx.json";

    public static final String FILE_XML_JB = "./data/data_jb.xml";

    public static final String FILE_JSON_JB = "./data/data_jb.json";

}
