package ru.ermolaev.tm.service;

import org.springframework.stereotype.Service;
import ru.ermolaev.tm.api.service.IPropertyService;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Service
public class PropertyService implements IPropertyService {

    private final String file = "/application.properties";

    private final Properties properties = new Properties();

    {
        try (final InputStream inputStream = PropertyService.class.getResourceAsStream(file)) {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getServerHost() {
        final String propertyHost = properties.getProperty("server.host");
        final String envHost = System.getProperty("server.host");
        if (envHost != null) return envHost;
        return propertyHost;
    }

    @Override
    public Integer getServerPort() {
        final String propertyPort = properties.getProperty("server.port");
        final String envPort = System.getProperty("server.port");
        String value = propertyPort;
        if (envPort != null) value = envPort;
        return Integer.parseInt(value);
    }

    @Override
    public String getSessionSalt() {
        return properties.getProperty("session.salt");
    }


    @Override
    public Integer getSessionCycle() {
        return Integer.parseInt(properties.getProperty("session.cycle"));
    }

    @Override
    public String getDatabaseDriver() {
        final String propertyDriver = properties.getProperty("database.driver");
        final String envDriver = System.getProperty("database.driver");
        if (envDriver != null) return envDriver;
        return propertyDriver;
    }

    @Override
    public String getDatabaseType() {
        final String propertyType = properties.getProperty("database.type");
        final String envType = System.getProperty("database.type");
        if (envType != null) return envType;
        return propertyType;
    }

    @Override
    public String getDatabaseHost() {
        final String propertyHost = properties.getProperty("database.host");
        final String envHost = System.getProperty("database.host");
        if (envHost != null) return envHost;
        return propertyHost;
    }

    @Override
    public String getDatabasePort() {
        final String propertyPort = properties.getProperty("database.port");
        final String envPort = System.getProperty("database.port");
        if (envPort != null) return envPort;
        return propertyPort;
    }

    @Override
    public String getDatabaseName() {
        final String propertyName = properties.getProperty("database.name");
        final String envName = System.getProperty("database.name");
        if (envName != null) return envName;
        return propertyName;
    }

    @Override
    public String getDatabaseUsername() {
        final String propertyUsername = properties.getProperty("database.username");
        final String envUsername = System.getProperty("database.username");
        if (envUsername != null) return envUsername;
        return propertyUsername;
    }

    @Override
    public String getDatabasePassword() {
        final String propertyPassword = properties.getProperty("database.password");
        final String envPassword = System.getProperty("database.password");
        if (envPassword != null) return envPassword;
        return propertyPassword;
    }

    @Override
    public String getJdbcUrl() {
        final StringBuilder stringBuilder = new StringBuilder(getDatabaseType())
                .append("://")
                .append(getDatabaseHost())
                .append(":")
                .append(getDatabasePort())
                .append("/")
                .append(getDatabaseName());
        return stringBuilder.toString();
    }

}
