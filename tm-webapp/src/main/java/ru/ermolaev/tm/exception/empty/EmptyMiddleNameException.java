package ru.ermolaev.tm.exception.empty;

import ru.ermolaev.tm.exception.AbstractException;

public final class EmptyMiddleNameException extends AbstractException {

    public EmptyMiddleNameException() {
        super("Error! Middle name is empty.");
    }

}
