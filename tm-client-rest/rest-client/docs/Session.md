
# Session

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**startTime** | **Long** |  |  [optional]
**signature** | **String** |  |  [optional]
**user** | [**User**](User.md) |  |  [optional]



